#include "pessoa.hpp"
#include "professor.hpp"
#include <iostream>
using namespace std;

Professor::Professor(){
	setNome("");
	setMatricula("");
	setIdade(0);
	setSexo("");
	setTelefone("");
	setFormacao("");
	setSala(0);
	setSalario(1.5);
	
}

Professor::~Professor(){}

void Professor::setFormacao(string formacao){
	this->formacao = formacao;
}

void Professor::setSala(int sala){
	this->sala = sala;
}

void Professor::setSalario(double salario){
	this->salario = salario;
}

string Professor::getFormacao(){
	return formacao;
}

int Professor::getSala(){
	return sala;
}

double Professor::getSalario(){
	return salario;
}

void Professor::imprimeDadosProfessor(){
   cout << "Dados do Professor" << endl;
   imprimeDados();
   cout << "Formação: " << getFormacao() << endl;
   cout << "Salário: " << getSalario() << endl;
   cout << "Sala: " << getSala() << endl;
}
